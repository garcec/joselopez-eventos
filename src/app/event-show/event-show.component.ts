import { Component, OnInit } from '@angular/core';
import { IEvent } from 'interfaces/i-event';

@Component({
  selector: 'app-event-show',
  templateUrl: './event-show.component.html',
  styleUrls: ['./event-show.component.css']
})
export class EventShowComponent implements OnInit {
events: IEvent[]=[{
  title:'la mayor fiesta',
  image: 'efhejdhx.jpg',
  date: new Date ('2018-07-08'),
  description: 'ekjhfjehd',
  price: 70
}, {
  title:'la peor fiesta',
  image: 'hjggg.jpg',
  date: new Date ('2018-08-09'),
  description: 'lo peor de lo peor',
  price: 5
}];
  constructor() { }

  ngOnInit() {
  }

}
